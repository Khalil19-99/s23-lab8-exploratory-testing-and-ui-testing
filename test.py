from selenium import webdriver

# initialize the driver
driver = webdriver.Chrome()

# navigate to the GitHub homepage
driver.get('https://github.com/')

# click the "Sign in" button
driver.find_element_by_link_text('Sign in').click()

# enter valid login credentials
username_input = driver.find_element_by_name('login')
username_input.send_keys('your_github_username')

password_input = driver.find_element_by_name('password')
password_input.send_keys('your_github_password')

# click the "Sign in" button
driver.find_element_by_name('commit').click()

# verify that the user is logged in
assert 'GitHub' in driver.title

# close the browser window
driver.quit()
